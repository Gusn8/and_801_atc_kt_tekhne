package com.miramicodigo.sqlite.ui

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.widget.AdapterView
import android.view.ContextMenu
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ListView
import com.miramicodigo.sqlite.R
import com.miramicodigo.sqlite.db.DatabaseAdapter
import com.miramicodigo.sqlite.adapter.ListaAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), AdapterView.OnItemClickListener {

    private var adaptadorLista: ListaAdapter? = null
    private var db: DatabaseAdapter? = null
    private val ids = ArrayList<Long>()

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        title = "SQLite"

        lvLista!!.onItemClickListener = this
        adaptadorLista = ListaAdapter(this)
        lvLista!!.adapter = adaptadorLista
        registerForContextMenu(lvLista)



    }

    override fun onStart() {
        super.onStart()



    }

    fun cargarDatosLista() {



    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.activity_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_adicionar -> {



            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenu.ContextMenuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo)
        menuInflater.inflate(R.menu.item_lista, menu)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        val info = item.menuInfo as AdapterView.AdapterContextMenuInfo
        val index = info.position
        when (item.itemId) {
            R.id.menu_editar -> {



            }
            R.id.menu_eliminar -> {



            }
        }
        return super.onContextItemSelected(item)
    }

    override fun onStop() {
        super.onStop()


    }

    override fun onItemClick(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {



    }

}
