package com.miramicodigo.notificaciones

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import android.support.v4.app.NotificationCompat
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.AsyncTask
import android.graphics.BitmapFactory
import android.view.View
import android.app.NotificationManager
import android.app.NotificationChannel
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import android.R.attr.banner
import android.app.Notification
import android.media.RingtoneManager

class MainActivity : AppCompatActivity(), View.OnClickListener{

    private var mNotificationManager: NotificationManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)




        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel("my_channel_01",
                    "Channel human readable title", NotificationManager.IMPORTANCE_DEFAULT)
            mNotificationManager!!.createNotificationChannel(channel)
        }
        btnNotificacionSimple.setOnClickListener(this)
        btnNotificacionGrande.setOnClickListener(this)
        btnNotificacionProgreso.setOnClickListener(this)
        btnNotificacionAcciones.setOnClickListener(this)
        btnNotificacionImagenGrande.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnNotificacionSimple -> createSimpleNotification(this)
            R.id.btnNotificacionGrande -> createExpandableNotification(this)
            R.id.btnNotificacionImagenGrande -> createBigImageNotification(this)
            R.id.btnNotificacionProgreso -> createProgressNotification(this)
            R.id.btnNotificacionAcciones -> createButtonNotification(this)
        }
    }

    fun createSimpleNotification(context: Context) {



    }

    fun createExpandableNotification(context: Context) {



    }

    fun createBigImageNotification(context: Context) {



    }

    fun createButtonNotification(context: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {



        } else {
            Toast.makeText(context, "Necesitas una version mas alta", Toast.LENGTH_LONG).show()
        }
    }

    fun createProgressNotification(context: Context) {
        val progresID = Random().nextInt(1000)



        var downloadTask = object : AsyncTask<Int, Int, Int>() {
            override fun onPreExecute() {
                super.onPreExecute()


            }

            override fun doInBackground(vararg p0: Int?): Int? {
                try {
                    Thread.sleep(5000)
                    var i = 0
                    while (i < 101) {

                    }

                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }

                return null
            }

            override fun onPostExecute(integer: Int?) {
                super.onPostExecute(integer)


            }
        }
        downloadTask.execute()
    }

}
