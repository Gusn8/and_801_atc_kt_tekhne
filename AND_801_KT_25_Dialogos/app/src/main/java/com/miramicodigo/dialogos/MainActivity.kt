package com.miramicodigo.dialogos

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import android.support.v7.app.AlertDialog
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*
import android.widget.EditText
import android.widget.CheckBox

class MainActivity : AppCompatActivity() {

    private var btnCrear: Button? = null
    private var btnEntrar: Button? = null
    private var etNombre: EditText? = null
    private var etContrasenia: EditText? = null
    private var posicion: Int = 0
    private var res: String? = null
    private var cbRecordar: CheckBox? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnDialogoSimple.setOnClickListener{

        }

        btnDialogoConLista.setOnClickListener {

        }

        btnDialogoConListaRadio.setOnClickListener {

        }

        btnDialogoConListaCheckbox.setOnClickListener {

        }

        btnDialogoPersonalizado.setOnClickListener {

        }

    }

    fun crearDialogoSimple(): AlertDialog {

        return null!!
    }


    fun crearDialogoConLista(): AlertDialog {

        return null!!
    }

    fun crearDialogoConListaRadio(): AlertDialog {

        return null!!
    }

    fun crearDialogoCheckBox(): AlertDialog {

        return null!!
    }

    fun crearDialogoPersonalizado(): AlertDialog {

        return null!!
    }
}
